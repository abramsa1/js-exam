class Tab {
  constructor(tabArr) {
    this.tab = tabArr;
    document.addEventListener('keydown', this.keyDownHandler.bind(this));
  }
  render() {
    const fragment = document.createDocumentFragment();
    const tabContainer = document.createElement('div');
    
    const tabHeader = document.createElement('div');
    tabHeader.classList.add('tab__items');
    tabHeader.addEventListener('click', this.activateTab);

    this.tab.forEach((item, index) => {
      const tabTitle = this.createTabHeaderItem(item.title, index);
      tabHeader.appendChild(tabTitle);

      const tabItem = this.createTabContentItem(item.data, index);
      tabContainer.appendChild(tabItem);
    })

    fragment.append(tabHeader, tabContainer);
    return fragment;
  }

  createTabHeaderItem(data, index) {
    const tabTitle = document.createElement('div');
    tabTitle.textContent = data;
    tabTitle.tabIndex = index + 1;
    if (index === 0) {
      tabTitle.classList.add('tab__item', '_current', 'outline');
    } else {
      tabTitle.classList.add('tab__item', 'outline');
    }
    return tabTitle
  }

  createTabContentItem(data, index) {
    const tabItem = document.createElement('div');
    tabItem.textContent = data;
    if (index === 0) {
      tabItem.classList.add('tab__content');
    } else {
      tabItem.classList.add('tab__content', 'hide');
    }
    return tabItem;
  }

  activateTab({ target }) {
    const tabContainer = target.closest('#tab');
    if (tabContainer) {
      const titles = tabContainer.querySelectorAll('.tab__item');
      const contents = tabContainer.querySelectorAll('.tab__content');

      titles.forEach((element, index) => {
        if (element !== target) {
          element.classList.remove('_current');
          contents[index].classList.add('hide');
        } else {
          element.classList.add('_current');
          contents[index].classList.remove('hide');
        }
      })
    }
  }

  keyDownHandler(e) {
    const focusTitle = document.querySelector('.tab__item:focus');
    if (focusTitle) {
      if (e.code === 'Enter') {
        this.activateTab({ target: focusTitle });
      }
      if (e.code === 'ArrowLeft' || e.code === 'ArrowDown') {
        const leftElement = focusTitle.previousElementSibling;
        if (leftElement) {
          leftElement.focus();
        }
      }
      if (e.code === 'ArrowRight' || e.code === 'ArrowUp') {
        const rightElement = focusTitle.nextElementSibling;
        if (rightElement) {
          rightElement.focus();
        }
      }
    }
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render())
